# The code for changing pages was derived from: http://stackoverflow.com/questions/7546050/switch-between-two-frames-in-tkinter
# License: http://creativecommons.org/licenses/by-sa/3.0/	

import tkinter as tk
import io
from trainingAndTesting import Training
from databasemodule import Database
from tkinter.filedialog import askopenfilename

try:
    import Tkinter as tk
except ImportError:
    import tkinter as tk

try:
    import ttk
    py3 = False
except ImportError:
    import tkinter.ttk as ttk
    py3 = True

import UIforPOS_support

LARGE_FONT= ("Verdana", 12)


class SeaofBTCapp(tk.Tk):

    def __init__(self, *args, **kwargs):

        tk.Tk.__init__(self, *args, **kwargs)
        container = tk.Frame(self)

        container.pack(side="top", fill="both", expand = True)

        container.grid_rowconfigure(10, weight=1)
        container.grid_columnconfigure(10, weight=1)

        self.frames = {}

        for F in (StartPage, PageOne, PageTwo):

            frame = F(container, self)

            self.frames[F] = frame

            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(StartPage)

    def show_frame(self, cont):

        frame = self.frames[cont]
        frame.tkraise()
    def create_Toplevel1(root):
        global w, w_win, rt
        rt = root
        w = tk.Toplevel (root)
        top = Toplevel1 (w)
        UIforPOS_support.init(w, top)
        return (w, top)
    def create_Toplevel2(root):
        global w, w_win, rt
        rt = root
        w = tk.Toplevel (root)
        top = Toplevel2 (w)
        UIforPOS_support.init(w, top)
        return (w, top)
    def create_Toplevel3(root):
        global w, w_win, rt
        rt = root
        w = tk.Toplevel (root)
        top = Toplevel3 (w)
        UIforPOS_support.init(w, top)
        return (w, top)
        
class StartPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent)

        label = tk.Label(self, text="Start Page", font=LARGE_FONT)
        label.pack(pady=10,padx=10)

       
        button3 = tk.Button(self, text="POS Tag",
                            command =lambda: controller.create_Toplevel1())
        button3.pack()
        button4 = tk.Button(self, text="Start page",
                            command =lambda: controller.create_Toplevel2())
        button4.pack()
        button5 = tk.Button(self, text="Data Collection page",
                            command =lambda: controller.create_Toplevel3())
        button5.pack()

class PageOne(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Page One!!!", font=LARGE_FONT)
        label.pack(pady=10,padx=10)

        button1 = tk.Button(self, text="Back to Home",
                            command=lambda: controller.show_frame(StartPage))
        button1.pack()

        button2 = tk.Button(self, text="Page Two",
                            command=lambda: controller.show_frame(PageTwo))
        button2.pack()


class PageTwo(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Page Two!!!", font=LARGE_FONT)
        label.pack(pady=10,padx=10)

        button1 = tk.Button(self, text="Back to Home",
                            command=lambda: controller.show_frame(StartPage))
        button1.pack()

        button2 = tk.Button(self, text="Page One",
                            command=lambda: controller.show_frame(PageOne))
        button2.pack()




class Toplevel3:
     #self.Message1.configure(text=output)
    def __init__(self, top=None):
        '''This class configures and populates the toplevel window.
           top is the toplevel containing window.'''
        _bgcolor = '#d9d9d9'  # X11 color: 'gray85'
        _fgcolor = '#000000'  # X11 color: 'black'
        _compcolor = '#d9d9d9' # X11 color: 'gray85'
        _ana1color = '#d9d9d9' # X11 color: 'gray85' 
        _ana2color = '#d9d9d9' # X11 color: 'gray85' 
        font10 = "-family {Segoe UI Symbol} -size 12 -weight normal "  \
            "-slant roman -underline 0 -overstrike 0"
        font11 = "-family {Bookman Old Style} -size 18 -weight normal "  \
            "-slant italic -underline 1 -overstrike 0"
        font12 = "-family {Segoe UI Emoji} -size 12 -weight normal "  \
            "-slant roman -underline 0 -overstrike 0"
        font14 = "-family {Ekushey Lohit} -size 20 -weight normal "  \
            "-slant italic -underline 1 -overstrike 0"
        font18 = "-family SutonnyMJ -size 12 -weight normal -slant "  \
            "roman -underline 0 -overstrike 0"
        font19 = "-family SutonnyMJ -size 12 -weight bold -slant "  \
            "italic -underline 0 -overstrike 0"
        font9 = "-family {Segoe UI Semibold} -size 12 -weight bold "  \
            "-slant roman -underline 0 -overstrike 0"

        top.geometry("1091x670+110+34")
        top.title("Bengali Part-of-Speech Tagger")
        top.configure(borderwidth="5")
        top.configure(background="#535451")
        top.configure(highlightbackground="#d9d9d9")
        top.configure(highlightcolor="#81ff7d")

        self.menubar = tk.Menu(top,font="TkMenuFont",bg=_bgcolor,fg=_fgcolor)
        top.configure(menu = self.menubar)

        self.Label1 = tk.Label(top)
        self.Label1.place(relx=0.412, rely=0.0, height=53, width=153)
        self.Label1.configure(activebackground="#a3a5a0")
        self.Label1.configure(activeforeground="#000000")
        self.Label1.configure(background="#535451")
        self.Label1.configure(disabledforeground="#3b2fa3")
        self.Label1.configure(font=('Lohit Bengali', 18))
        self.Label1.configure(foreground="#63ff69")
        self.Label1.configure(highlightbackground="#d9d9d9")
        self.Label1.configure(highlightcolor="#000000")
        self.Label1.configure(text='''পদ প্রকরণ''')
        
        self.Label1_7 = tk.Label(top)
        self.Label1_7.place(relx=0.284, rely=0.09, height=43, width=423)
        self.Label1_7.configure(activebackground="#a3a5a0")
        self.Label1_7.configure(activeforeground="#000000")
        self.Label1_7.configure(background="#535451")
        self.Label1_7.configure(disabledforeground="#3b2fa3")
        self.Label1_7.configure(font=font11)
        self.Label1_7.configure(foreground="#63ff69")
        self.Label1_7.configure(highlightbackground="#d9d9d9")
        self.Label1_7.configure(highlightcolor="#000000")
        self.Label1_7.configure(text='''Part-of-Speech Tagger(Bengali)''')
        
        self.Button2_11 = tk.Button(top)
        self.Button2_11.place(relx=0.321, rely=0.224, height=136, width=336)
        self.Button2_11.configure(activebackground="#d9d9d9")
        self.Button2_11.configure(activeforeground="#000000")
        self.Button2_11.configure(background="#d8112c")
        self.Button2_11.configure(disabledforeground="#a3a3a3")
        self.Button2_11.configure(font=font14)
        self.Button2_11.configure(foreground="#fff8f7")
        self.Button2_11.configure(highlightbackground="#d9d9d9")
        self.Button2_11.configure(highlightcolor="black")
        self.Button2_11.configure(pady="0")
        self.Button2_11.configure(text='''ডাটা কালেকশন''')
        self.Button2_11.configure(width=336)


class Toplevel2:
     #self.Message1.configure(text=output)
    def __init__(self, top=None):
        '''This class configures and populates the toplevel window.
           top is the toplevel containing window.'''
        _bgcolor = '#d9d9d9'  # X11 color: 'gray85'
        _fgcolor = '#000000'  # X11 color: 'black'
        _compcolor = '#d9d9d9' # X11 color: 'gray85'
        _ana1color = '#d9d9d9' # X11 color: 'gray85' 
        _ana2color = '#d9d9d9' # X11 color: 'gray85' 
        font10 = "-family {Segoe UI Symbol} -size 12 -weight normal "  \
            "-slant roman -underline 0 -overstrike 0"
        font11 = "-family {Bookman Old Style} -size 18 -weight normal "  \
            "-slant italic -underline 1 -overstrike 0"
        font12 = "-family {Segoe UI Emoji} -size 12 -weight normal "  \
            "-slant roman -underline 0 -overstrike 0"
        font14 = "-family {Ekushey Lohit} -size 20 -weight normal "  \
            "-slant italic -underline 1 -overstrike 0"
        font18 = "-family SutonnyMJ -size 12 -weight normal -slant "  \
            "roman -underline 0 -overstrike 0"
        font19 = "-family SutonnyMJ -size 12 -weight bold -slant "  \
            "italic -underline 0 -overstrike 0"
        font9 = "-family {Segoe UI Semibold} -size 12 -weight bold "  \
            "-slant roman -underline 0 -overstrike 0"

        top.geometry("1091x670+110+34")
        top.title("Bengali Part-of-Speech Tagger")
        top.configure(borderwidth="5")
        top.configure(background="#535451")
        top.configure(highlightbackground="#d9d9d9")
        top.configure(highlightcolor="#81ff7d")

        self.menubar = tk.Menu(top,font="TkMenuFont",bg=_bgcolor,fg=_fgcolor)
        top.configure(menu = self.menubar)

        self.Label1 = tk.Label(top)
        self.Label1.place(relx=0.412, rely=0.0, height=53, width=153)
        self.Label1.configure(activebackground="#a3a5a0")
        self.Label1.configure(activeforeground="#000000")
        self.Label1.configure(background="#535451")
        self.Label1.configure(disabledforeground="#3b2fa3")
        self.Label1.configure(font=('Lohit Bengali', 18))
        self.Label1.configure(foreground="#63ff69")
        self.Label1.configure(highlightbackground="#d9d9d9")
        self.Label1.configure(highlightcolor="#000000")
        self.Label1.configure(text='''পদ প্রকরণ''')
        
        self.Label1_7 = tk.Label(top)
        self.Label1_7.place(relx=0.284, rely=0.09, height=43, width=423)
        self.Label1_7.configure(activebackground="#a3a5a0")
        self.Label1_7.configure(activeforeground="#000000")
        self.Label1_7.configure(background="#535451")
        self.Label1_7.configure(disabledforeground="#3b2fa3")
        self.Label1_7.configure(font=font11)
        self.Label1_7.configure(foreground="#63ff69")
        self.Label1_7.configure(highlightbackground="#d9d9d9")
        self.Label1_7.configure(highlightcolor="#000000")
        self.Label1_7.configure(text='''Part-of-Speech Tagger(Bengali)''')
        
        self.Button2 = tk.Button(top)
        self.Button2.place(relx=0.147, rely=0.328, height=26, width=112)
        self.Button2.configure(activebackground="#d9d9d9")
        self.Button2.configure(activeforeground="#000000")
        self.Button2.configure(background="#d8112c")
        self.Button2.configure(disabledforeground="#a3a3a3")
        self.Button2.configure(font=font18)
        self.Button2.configure(foreground="#fff8f7")
        self.Button2.configure(highlightbackground="#d9d9d9")
        self.Button2.configure(highlightcolor="black")
        self.Button2.configure(pady="0")
        self.Button2.configure(text='''পদ প্রকরণ''')
        
        
        self.Button2_11 = tk.Button(top)
        self.Button2_11.place(relx=0.66, rely=0.328, height=26, width=166)
        self.Button2_11.configure(activebackground="#d9d9d9")
        self.Button2_11.configure(activeforeground="#000000")
        self.Button2_11.configure(background="#d8112c")
        self.Button2_11.configure(disabledforeground="#a3a3a3")
        self.Button2_11.configure(font=font18)
        self.Button2_11.configure(foreground="#fff8f7")
        self.Button2_11.configure(highlightbackground="#d9d9d9")
        self.Button2_11.configure(highlightcolor="black")
        self.Button2_11.configure(pady="0")
        self.Button2_11.configure(text='''ডাটা কালেকশন''')

class Toplevel1:
    def highlight(self,words,tags):
        start = 1.0
        wordsSize = len(words)
        #print(words)
        #print(tags)
        #pos = self.Text1.search('নি', start, stopindex='end')
        self.Text1.tag_config('noun', background="#ff1929", foreground='black')
        self.Text1.tag_config('pronoun', background="#ffcd45", foreground='black')
        self.Text1.tag_config('adjective', background="#d836d8", foreground='black')
        self.Text1.tag_config('verb', background="#5f5fd8", foreground='black')
        self.Text1.tag_config('adverb', background="#34d8bd", foreground='black')
        self.Text1.tag_config('determiner', background="#87dd16", foreground='black')
        self.Text1.tag_config('postposition', background="#d8a0a7", foreground='black')
        self.Text1.tag_config('conjunction', background="#b39ed8", foreground='black')
        self.Text1.tag_config('participle', background="#11169b", foreground='black')
        self.Text1.tag_config('quantifier', background="#d8327f", foreground='black')
        self.Text1.tag_config('punctuation', background="#b1b1d8", foreground='black')
        self.Text1.tag_config('other', background="#6a996b", foreground='black')
        
        #print(self.Text1.get("1.0",'end-1c'))                     
        for i in range (wordsSize):
            wordForTag = words[i]
            tagForTag = tags[i]
            length = len(wordForTag)
            #print(wordForTag)
            #print(start)
        
            pos = self.Text1.search(wordForTag, start, stopindex='end')
            #print(pos.split('.'))
            row,col = pos.split('.')
            en = int(col) + length
            en = row + '.' + str(en)
            self.Text1.tag_add(tagForTag,pos,en)
            start = en
            #pos = self.Text1.search('নি', start, stopindex='end')
        '''while pos:
            length = len('নি')
            row,col = pos.split('.')
            en = int(col) + length
            en = row + '.' + str(en)
            self.Text1.tag_add('highlight',pos,en)
            start = en
            pos = self.Text1.search('নি', start, stopindex='end')'''
        
    def insertSentence(self):
        filename = askopenfilename()
        #print(filename)
        
        with io.open(filename, "r",encoding='utf8') as myfile:
            tr_str = myfile.read()
        #print(tr_str)
        self.Text1.insert(1.0,tr_str)
        
    def posTag(self):
        tupuls = Training().training("data.txt")
        tag2tag = tupuls[0]
        word2tag = tupuls[1]
        word2tagBaseline = tupuls[2]
        sentenceForTag = self.Text1.get("1.0",'end-1c')
        #print(sentenceForTag)
        output = Training().fn_assign_POS_tags(sentenceForTag,tag2tag,word2tag,word2tagBaseline)
        
        #outputTag = output[0]
        #outputWords = output[1]
        #output = Training().fn_assign_POS_tags("input.txt",tag2tag,word2tag,word2tagBaseline)
        #print(output[0])
        self.highlight(output[0],output[1])
        
        
        #self.Message1.configure(text=output)
    def __init__(self, top=None):
        '''This class configures and populates the toplevel window.
           top is the toplevel containing window.'''
        _bgcolor = '#d9d9d9'  # X11 color: 'gray85'
        _fgcolor = '#000000'  # X11 color: 'black'
        _compcolor = '#d9d9d9' # X11 color: 'gray85'
        _ana1color = '#d9d9d9' # X11 color: 'gray85' 
        _ana2color = '#d9d9d9' # X11 color: 'gray85' 
        font10 = "-family {Segoe UI Symbol} -size 12 -weight normal "  \
            "-slant roman -underline 0 -overstrike 0"
        font11 = "-family {Bookman Old Style} -size 18 -weight normal "  \
            "-slant italic -underline 1 -overstrike 0"
        font12 = "-family {Segoe UI Emoji} -size 12 -weight normal "  \
            "-slant roman -underline 0 -overstrike 0"
        font14 = "-family {Ekushey Lohit} -size 20 -weight normal "  \
            "-slant italic -underline 1 -overstrike 0"
        font18 = "-family SutonnyMJ -size 12 -weight normal -slant "  \
            "roman -underline 0 -overstrike 0"
        font19 = "-family SutonnyMJ -size 12 -weight bold -slant "  \
            "italic -underline 0 -overstrike 0"
        font9 = "-family {Segoe UI Semibold} -size 12 -weight bold "  \
            "-slant roman -underline 0 -overstrike 0"

        top.geometry("1091x670+110+34")
        top.title("Bengali Part-of-Speech Tagger")
        top.configure(borderwidth="5")
        top.configure(background="#535451")
        top.configure(highlightbackground="#d9d9d9")
        top.configure(highlightcolor="#81ff7d")

        self.menubar = tk.Menu(top,font="TkMenuFont",bg=_bgcolor,fg=_fgcolor)
        top.configure(menu = self.menubar)

        self.Label1 = tk.Label(top)
        self.Label1.place(relx=0.412, rely=0.0, height=53, width=153)
        self.Label1.configure(activebackground="#a3a5a0")
        self.Label1.configure(activeforeground="#000000")
        self.Label1.configure(background="#535451")
        self.Label1.configure(disabledforeground="#3b2fa3")
        self.Label1.configure(font=('Lohit Bengali', 18))
        self.Label1.configure(foreground="#63ff69")
        self.Label1.configure(highlightbackground="#d9d9d9")
        self.Label1.configure(highlightcolor="#000000")
        self.Label1.configure(text='''পদ প্রকরণ''')

        self.Label2 = tk.Label(top)
        self.Label2.place(relx=0.009, rely=0.149, height=41, width=581)
        self.Label2.configure(activebackground="#f9f9f9")
        self.Label2.configure(activeforeground="#2937ff")
        self.Label2.configure(background="#535451")
        self.Label2.configure(disabledforeground="#a3a3a3")
        self.Label2.configure(font=font18)
        self.Label2.configure(foreground="#ffffff")
        self.Label2.configure(highlightbackground="#d9d9d9")
        self.Label2.configure(highlightcolor="black")
        self.Label2.configure(text='''বক্সে একটি সম্পুর্ণ বাক্য লিখুন(একক বাক্য নয়) এবং 'পদ প্রকরণ' বাটনে ক্লিক করুন''') 

        self.Button1 = tk.Button(top)
        self.Button1.place(relx=0.33, rely=0.567, height=25, width=93)
        self.Button1.configure(activebackground="#d9d9d9")
        self.Button1.configure(activeforeground="#000000")
        self.Button1.configure(background="#9ba5aa")
        self.Button1.configure(disabledforeground="#a3a3a3")
        self.Button1.configure(font=font19)
        self.Button1.configure(foreground="#1e4230")
        self.Button1.configure(highlightbackground="#d9d9d9")
        self.Button1.configure(highlightcolor="#000000")
        self.Button1.configure(pady="0")
        self.Button1.configure(text='''পদ প্রকরণ''')
        self.Button1.configure(width=93)
        self.Button1.configure(command=self.posTag)

        self.Label4 = tk.Label(top)
        self.Label4.place(relx=0.018, rely=0.642, height=21, width=746)
        self.Label4.configure(activebackground="#f9f9f9")
        self.Label4.configure(activeforeground="black")
        self.Label4.configure(background="#878787")
        self.Label4.configure(disabledforeground="#a3a3a3")
        self.Label4.configure(font=font12)
        self.Label4.configure(foreground="#f0fff4")
        self.Label4.configure(highlightbackground="#d9d9d9")
        self.Label4.configure(highlightcolor="black")
        self.Label4.configure(text='''Don't worry about the result. Computers make mistakes too!''')
        self.Label4.configure(width=746)

        self.Button2 = tk.Button(top)
        self.Button2.place(relx=0.275, rely=0.925, height=26, width=112)
        self.Button2.configure(activebackground="#d9d9d9")
        self.Button2.configure(activeforeground="#000000")
        self.Button2.configure(background="#d8112c")
        self.Button2.configure(disabledforeground="#a3a3a3")
        self.Button2.configure(font=font18)
        self.Button2.configure(foreground="#fff8f7")
        self.Button2.configure(highlightbackground="#d9d9d9")
        self.Button2.configure(highlightcolor="black")
        self.Button2.configure(pady="0")
        self.Button2.configure(text='''প্রস্থান করুন''')
        self.Button2.configure(width=112)
        
        self.Label3 = tk.Label(top)
        self.Label3.place(relx=0.733, rely=0.164, height=21, width=226)
        self.Label3.configure(activebackground="#f9f9f9")
        self.Label3.configure(activeforeground="black")
        self.Label3.configure(background="#ff1929")
        self.Label3.configure(disabledforeground="#a3a3a3")
        self.Label3.configure(font=font10)
        self.Label3.configure(foreground="#000000")
        self.Label3.configure(highlightbackground="#d9d9d9")
        self.Label3.configure(highlightcolor="black")
        self.Label3.configure(justify='left')
        self.Label3.configure(text='''Noun''')

        self.Label3_1 = tk.Label(top)
        self.Label3_1.place(relx=0.733, rely=0.299, height=21, width=226)
        self.Label3_1.configure(activebackground="#f9f9f9")
        self.Label3_1.configure(activeforeground="black")
        self.Label3_1.configure(background="#5f5fd8")
        self.Label3_1.configure(disabledforeground="#a3a3a3")
        self.Label3_1.configure(font=font10)
        self.Label3_1.configure(foreground="#000000")
        self.Label3_1.configure(highlightbackground="#d9d9d9")
        self.Label3_1.configure(highlightcolor="black")
        self.Label3_1.configure(text='''Verb''')

        self.Label3_2 = tk.Label(top)
        self.Label3_2.place(relx=0.733, rely=0.254, height=21, width=226)
        self.Label3_2.configure(activebackground="#f9f9f9")
        self.Label3_2.configure(activeforeground="black")
        self.Label3_2.configure(background="#d836d8")
        self.Label3_2.configure(disabledforeground="#a3a3a3")
        self.Label3_2.configure(font=font10)
        self.Label3_2.configure(foreground="#000000")
        self.Label3_2.configure(highlightbackground="#d9d9d9")
        self.Label3_2.configure(highlightcolor="black")
        self.Label3_2.configure(text='''Adjective''')

        self.Label3_3 = tk.Label(top)
        self.Label3_3.place(relx=0.733, rely=0.209, height=21, width=226)
        self.Label3_3.configure(activebackground="#f9f9f9")
        self.Label3_3.configure(activeforeground="black")
        self.Label3_3.configure(background="#ffcd45")
        self.Label3_3.configure(disabledforeground="#a3a3a3")
        self.Label3_3.configure(font=font10)
        self.Label3_3.configure(foreground="#000000")
        self.Label3_3.configure(highlightbackground="#d9d9d9")
        self.Label3_3.configure(highlightcolor="#000000")
        self.Label3_3.configure(text='''Pronoun''')

        self.Label3_4 = tk.Label(top)
        self.Label3_4.place(relx=0.733, rely=0.433, height=21, width=226)
        self.Label3_4.configure(activebackground="#f9f9f9")
        self.Label3_4.configure(activeforeground="black")
        self.Label3_4.configure(background="#d8a0a7")
        self.Label3_4.configure(disabledforeground="#a3a3a3")
        self.Label3_4.configure(font=font10)
        self.Label3_4.configure(foreground="#000000")
        self.Label3_4.configure(highlightbackground="#d9d9d9")
        self.Label3_4.configure(highlightcolor="black")
        self.Label3_4.configure(text='''Postposition''')

        self.Label3_1 = tk.Label(top)
        self.Label3_1.place(relx=0.733, rely=0.388, height=21, width=226)
        self.Label3_1.configure(activebackground="#f9f9f9")
        self.Label3_1.configure(activeforeground="black")
        self.Label3_1.configure(background="#87dd16")
        self.Label3_1.configure(disabledforeground="#a3a3a3")
        self.Label3_1.configure(font=font10)
        self.Label3_1.configure(foreground="#000000")
        self.Label3_1.configure(highlightbackground="#d9d9d9")
        self.Label3_1.configure(highlightcolor="black")
        self.Label3_1.configure(text='''Determiner''')

        self.Label3_2 = tk.Label(top)
        self.Label3_2.place(relx=0.733, rely=0.343, height=21, width=226)
        self.Label3_2.configure(activebackground="#f9f9f9")
        self.Label3_2.configure(activeforeground="black")
        self.Label3_2.configure(background="#34d8bd")
        self.Label3_2.configure(disabledforeground="#a3a3a3")
        self.Label3_2.configure(font=font10)
        self.Label3_2.configure(foreground="#000000")
        self.Label3_2.configure(highlightbackground="#d9d9d9")
        self.Label3_2.configure(highlightcolor="black")
        self.Label3_2.configure(text='''Adverb''')

        self.Label3_5 = tk.Label(top)
        self.Label3_5.place(relx=0.733, rely=0.612, height=21, width=226)
        self.Label3_5.configure(activebackground="#f9f9f9")
        self.Label3_5.configure(activeforeground="black")
        self.Label3_5.configure(background="#b1b1d8")
        self.Label3_5.configure(disabledforeground="#a3a3a3")
        self.Label3_5.configure(font=font10)
        self.Label3_5.configure(foreground="#000000")
        self.Label3_5.configure(highlightbackground="#d9d9d9")
        self.Label3_5.configure(highlightcolor="black")
        self.Label3_5.configure(text='''Punctuatuion''')

        self.Label3_3 = tk.Label(top)
        self.Label3_3.place(relx=0.733, rely=0.567, height=21, width=226)
        self.Label3_3.configure(activebackground="#f9f9f9")
        self.Label3_3.configure(activeforeground="black")
        self.Label3_3.configure(background="#d8327f")
        self.Label3_3.configure(disabledforeground="#a3a3a3")
        self.Label3_3.configure(font=font10)
        self.Label3_3.configure(foreground="#000000")
        self.Label3_3.configure(highlightbackground="#d9d9d9")
        self.Label3_3.configure(highlightcolor="black")
        self.Label3_3.configure(text='''Quantifier''')

        self.Label3_4 = tk.Label(top)
        self.Label3_4.place(relx=0.733, rely=0.522, height=21, width=226)
        self.Label3_4.configure(activebackground="#f9f9f9")
        self.Label3_4.configure(activeforeground="black")
        self.Label3_4.configure(background="#11169b")
        self.Label3_4.configure(disabledforeground="#a3a3a3")
        self.Label3_4.configure(font=font10)
        self.Label3_4.configure(foreground="#000000")
        self.Label3_4.configure(highlightbackground="#d9d9d9")
        self.Label3_4.configure(highlightcolor="black")
        self.Label3_4.configure(text='''Participle''')

        self.Label3_6 = tk.Label(top)
        self.Label3_6.place(relx=0.733, rely=0.478, height=21, width=226)
        self.Label3_6.configure(activebackground="#f9f9f9")
        self.Label3_6.configure(activeforeground="black")
        self.Label3_6.configure(background="#b39ed8")
        self.Label3_6.configure(disabledforeground="#a3a3a3")
        self.Label3_6.configure(font=font10)
        self.Label3_6.configure(foreground="#000000")
        self.Label3_6.configure(highlightbackground="#d9d9d9")
        self.Label3_6.configure(highlightcolor="black")
        self.Label3_6.configure(text='''Conjunction''')

        self.Label3_7 = tk.Label(top)
        self.Label3_7.place(relx=0.733, rely=0.657, height=21, width=226)
        self.Label3_7.configure(activebackground="#f9f9f9")
        self.Label3_7.configure(activeforeground="black")
        self.Label3_7.configure(background="#6a996b")
        self.Label3_7.configure(disabledforeground="#a3a3a3")
        self.Label3_7.configure(font=font10)
        self.Label3_7.configure(foreground="#000000")
        self.Label3_7.configure(highlightbackground="#d9d9d9")
        self.Label3_7.configure(highlightcolor="black")
        self.Label3_7.configure(text='''Others''')

        self.Label1_7 = tk.Label(top)
        self.Label1_7.place(relx=0.284, rely=0.09, height=43, width=423)
        self.Label1_7.configure(activebackground="#a3a5a0")
        self.Label1_7.configure(activeforeground="#000000")
        self.Label1_7.configure(background="#535451")
        self.Label1_7.configure(disabledforeground="#3b2fa3")
        self.Label1_7.configure(font=font11)
        self.Label1_7.configure(foreground="#63ff69")
        self.Label1_7.configure(highlightbackground="#d9d9d9")
        self.Label1_7.configure(highlightcolor="#000000")
        self.Label1_7.configure(text='''Part-of-Speech Tagger(Bengali)''')

        self.Button2_11 = tk.Button(top)
        self.Button2_11.place(relx=0.596, rely=0.925, height=26, width=166)
        self.Button2_11.configure(activebackground="#d9d9d9")
        self.Button2_11.configure(activeforeground="#000000")
        self.Button2_11.configure(background="#d8112c")
        self.Button2_11.configure(disabledforeground="#a3a3a3")
        self.Button2_11.configure(font=font18)
        self.Button2_11.configure(foreground="#fff8f7")
        self.Button2_11.configure(highlightbackground="#d9d9d9")
        self.Button2_11.configure(highlightcolor="black")
        self.Button2_11.configure(pady="0")
        self.Button2_11.configure(text='''ডাটা কালেকশন করুন''')

        self.Text1 = tk.Text(top)
        self.Text1.place(relx=0.018, rely=0.209, relheight=0.334, relwidth=0.682)

        self.Text1.configure(background="white")
        self.Text1.configure(font="TkTextFont")
        self.Text1.configure(foreground="black")
        self.Text1.configure(highlightbackground="#d9d9d9")
        self.Text1.configure(highlightcolor="black")
        self.Text1.configure(insertbackground="black")
        self.Text1.configure(selectbackground="#c4c4c4")
        self.Text1.configure(selectforeground="black")
        self.Text1.configure(width=744)
        self.Text1.configure(wrap='word')
        
        self.S = tk.Scrollbar(top)
        
        self.Text1.configure(yscrollcommand=self.S.set)
        self.S.config(command=self.Text1.yview)
        
        
    
        self.Button1_9 = tk.Button(top)
        self.Button1_9.place(relx=0.55, rely=0.164, height=25, width=163)
        self.Button1_9.configure(activebackground="#d9d9d9")
        self.Button1_9.configure(activeforeground="#000000")
        self.Button1_9.configure(background="#9ba5aa")
        self.Button1_9.configure(disabledforeground="#a3a3a3")
        self.Button1_9.configure(font=font18)
        self.Button1_9.configure(foreground="#1e4230")
        self.Button1_9.configure(highlightbackground="#d9d9d9")
        self.Button1_9.configure(highlightcolor="#000000")
        self.Button1_9.configure(pady="0")
        self.Button1_9.configure(text='''ফাইল থেকে পরুন''')
        self.Button1_9.configure(width=163)
        self.Button1_9.configure(command=self.insertSentence)
        #self.Button1.configure(command=self.posTag)



app = SeaofBTCapp()
app.mainloop()