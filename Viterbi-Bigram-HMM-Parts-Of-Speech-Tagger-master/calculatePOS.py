import io
from __future__ import division #To avoid integer division
from operator import itemgetter






def fn_assign_POS_tags():
    with io.open("test.txt", "r",encoding='utf8') as myfile:
        te_str = myfile.read()
        words_list = te_str.split()
        num_words_test = len(words_list)
    output_li = ['']
    output_li*= num_words_test
    for i in range(num_words_test):
        if i==0:    #Accounting for the 1st word in the test document for the Viterbi
            di_transition_probs = dict2_tag_follow_tag_['।']
        else:
            di_transition_probs = dict2_tag_follow_tag_[output_li[i-1]]
            
        di_emission_probs = dict2_word_tag.get(words_list[i],'')
        if di_emission_probs=='':
            output_li[i]=''
        else:
            max_prod_prob = 0
            counter_trans = 0
            counter_emis =0
            prod_prob = 0
            while counter_trans < len(di_transition_probs) and counter_emis < len(di_emission_probs):
                tag_tr = di_transition_probs[counter_trans][0]
                tag_em = di_emission_probs[counter_emis][0]
                if tag_tr < tag_em:
                    counter_trans+=1
                elif tag_tr > tag_em:
                    counter_emis+=1
                else:
                    prod_prob = di_transition_probs[counter_trans][1] * di_emission_probs[counter_emis][1]
                    if prod_prob > max_prod_prob:
                        max_prod_prob = prod_prob
                        output_li[i] = tag_tr
                        #print "i=",i," and output=",output_li[i]
                    counter_trans+=1
                    counter_emis+=1    
        if output_li[i]=='': #In case there are no matching entries between the transition tags and emission tags, we choose the most frequent emission tag
            output_li[i] = max(di_emission_probs,key=itemgetter(1))[0]  
    return output_li
    fn_assign_POS_tags()
