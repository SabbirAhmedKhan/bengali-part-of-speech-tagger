#! /usr/bin/env python
#  -*- coding: utf-8 -*-
#
# GUI module generated by PAGE version 4.18
#  in conjunction with Tcl version 8.6
#    Nov 01, 2018 02:26:45 PM +06  platform: Windows NT

import sys
from tkinter import messagebox

#from __future__ import division #To avoid integer division
from operator import itemgetter
import io
from trainingAndTesting import Training

try:
    import Tkinter as tk
except ImportError:
    import tkinter as tk

try:
    import ttk
    py3 = False
except ImportError:
    import tkinter.ttk as ttk
    py3 = True

import UIforPOS_support

def vp_start_gui():
    '''Starting point when module is the main routine.'''
    global val, w, root
    root = tk.Tk()
    top = Toplevel1 (root)
    UIforPOS_support.init(root, top)
    root.mainloop()

w = None
def create_Toplevel1(root, *args, **kwargs):
    '''Starting point when module is imported by another program.'''
    global w, w_win, rt
    rt = root
    w = tk.Toplevel (root)
    top = Toplevel1 (w)
    UIforPOS_support.init(w, top, *args, **kwargs)
    return (w, top)

def destroy_Toplevel1():
    global w
    w.destroy()
    w = None

class Toplevel1:
    def posTag(self):
        tupuls = Training().training("data.txt")
        tag2tag = tupuls[0]
        word2tag = tupuls[1]
        word2tagBaseline = tupuls[2]
        sentenceForTag = self.Entry1.get()
        print(sentenceForTag)
        output = Training().fn_assign_POS_tags(sentenceForTag,tag2tag,word2tag,word2tagBaseline)
        #outputTag = output[0]
        #outputWords = output[1]
        #output = Training().fn_assign_POS_tags("input.txt",tag2tag,word2tag,word2tagBaseline)
        print(output)
        self.Message1.configure(text=output)

    def exitPOSTagger(self):
        msg = messagebox.askyesno("Exit page","Are you sure to exit?")
        if msg:
            exit()
    def __init__(self, top=None):
        '''This class configures and populates the toplevel window.
           top is the toplevel containing window.'''
        _bgcolor = '#d9d9d9'  # X11 color: 'gray85'
        _fgcolor = '#000000'  # X11 color: 'black'
        _compcolor = '#d9d9d9' # X11 color: 'gray85'
        _ana1color = '#d9d9d9' # X11 color: 'gray85' 
        _ana2color = '#d9d9d9' # X11 color: 'gray85' 

        top.geometry("851x613+214+83")
        top.title("Bengali Part-of-Speech Tagger")
        top.configure(background="#fcfff7")

        self.menubar = tk.Menu(top,font="TkMenuFont",bg=_bgcolor,fg=_fgcolor)
        top.configure(menu = self.menubar)

        self.Label1 = tk.Label(top)
        self.Label1.place(relx=0.094, rely=0.082, height=21, width=75)
        self.Label1.configure(background="#d9d9d9")
        self.Label1.configure(disabledforeground="#a3a3a3")
        self.Label1.configure(foreground="#000000")
        self.Label1.configure(text='''POS Tagging''')

        self.Label2 = tk.Label(top)
        self.Label2.place(relx=0.082, rely=0.033, height=51, width=741)
        self.Label2.configure(background="#d9d9d9")
        self.Label2.configure(disabledforeground="#a3a3a3")
        self.Label2.configure(foreground="#000000")
        self.Label2.configure(text='''Enter a complete sentence (no single words!) and click at "POS-tag!". The tagging works better when grammar and orthography are correct.''')
        self.Label2.configure(width=741)

        self.Label3 = tk.Label(top)
        self.Label3.place(relx=0.106, rely=0.294, height=21, width=28)
        self.Label3.configure(background="#d9d9d9")
        self.Label3.configure(disabledforeground="#a3a3a3")
        self.Label3.configure(foreground="#000000")
        self.Label3.configure(text='''Text''')
        self.Label3.configure(width=28)

        self.Entry1 = tk.Entry(top)
        self.Entry1.place(relx=0.106, rely=0.343,height=60, relwidth=0.804)
        self.Entry1.configure(background="white")
        self.Entry1.configure(disabledforeground="#a3a3a3")
        self.Entry1.configure(font="TkFixedFont")
        self.Entry1.configure(foreground="#000000")
        self.Entry1.configure(insertbackground="black")
        self.Entry1.configure(width=684)

        self.Button1 = tk.Button(top)
        self.Button1.place(relx=0.106, rely=0.489, height=24, width=59)
        self.Button1.configure(activebackground="#d9d9d9")
        self.Button1.configure(activeforeground="#000000")
        self.Button1.configure(background="#d9d9d9")
        self.Button1.configure(disabledforeground="#a3a3a3")
        self.Button1.configure(foreground="#000000")
        self.Button1.configure(highlightbackground="#d9d9d9")
        self.Button1.configure(highlightcolor="black")
        self.Button1.configure(pady="0")
        self.Button1.configure(text='''POS Tag!''')
        self.Button1.configure(command=self.posTag)

        self.Label4 = tk.Label(top)
        self.Label4.place(relx=0.106, rely=0.783, height=21, width=319)
        self.Label4.configure(background="#d9d9d9")
        self.Label4.configure(disabledforeground="#a3a3a3")
        self.Label4.configure(foreground="#000000")
        self.Label4.configure(text='''Don't worry about the result. Computer make mistakes too!''')

        self.Button2 = tk.Button(top)
        self.Button2.place(relx=0.529, rely=0.897, height=24, width=29)
        self.Button2.configure(activebackground="#d9d9d9")
        self.Button2.configure(activeforeground="#000000")
        self.Button2.configure(background="#d9d9d9")
        self.Button2.configure(disabledforeground="#a3a3a3")
        self.Button2.configure(foreground="#000000")
        self.Button2.configure(highlightbackground="#d9d9d9")
        self.Button2.configure(highlightcolor="black")
        self.Button2.configure(pady="0")
        self.Button2.configure(text='''Exit''')
        self.Button2.configure(command=self.exitPOSTagger)

        self.Message1 = tk.Message(top)
        self.Message1.place(relx=0.106, rely=0.571, relheight=0.119
                , relwidth=0.811)
        self.Message1.configure(background="#d9d9d9")
        self.Message1.configure(foreground="#000000")
        self.Message1.configure(highlightbackground="#d9d9d9")
        self.Message1.configure(highlightcolor="black")
        self.Message1.configure(text='''Message''')
        self.Message1.configure(width=690)
        

if __name__ == '__main__':
    vp_start_gui()