# -*- coding: utf-8 -*-
"""
Created on Fri Dec 14 08:24:20 2018

@author: Sabbir Ahmed Khan
"""
#!/usr/bin/python
# -*- coding: latin-1 -*-

import sqlite3
from sqlite3 import Error
import fpdf
import io
 
class Database:
    def create_connection(db_file):
   
        try:
            conn = sqlite3.connect(db_file)
            return conn
        except Error as e:
            print(e)
     
        return None
    
    def saveToDB(self,db_file,taggedSentence):
        try:
            conn = sqlite3.connect(db_file)
        except Error as e:
            print(e)
            
        c = conn.cursor()

# Create table
        c.execute('''CREATE TABLE if not exists sentenceTable
                     (sentences text)''')
        
        # Insert a row of data
        c.execute('INSERT INTO sentenceTable VALUES (?)', (taggedSentence,))
        
        # Save (commit) the changes
        conn.commit()
        print("successfully commited")
        
        # We can also close the connection if we are done with it.
        # Just be sure any changes have been committed or they will be lost.
        conn.close()
     
      

    
    def fetchFromDB(self, db_file,filename):
        try:
            conn = sqlite3.connect(db_file)
        except Error as e:
            print(e)
            
        
        
        cur = conn.cursor()
        cur.execute("SELECT * FROM sentenceTable")
     
        rows = cur.fetchall()
     
        
        with io.open(filename, "w",encoding='utf8') as myfile:
            for row in rows:
                sentence = ' '.join(row)
                myfile.write(sentence)
        #print(tr_str)
        
        '''for row in rows:
            pdf.write(5,str(row))
            print(row)
            pdf.ln()
        pdf.output("NLPData.pdf")'''
            
        conn.close()
     
     
    
     
 
