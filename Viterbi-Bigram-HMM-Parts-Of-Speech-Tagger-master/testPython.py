from __future__ import division #To avoid integer division
from operator import itemgetter
import io

class Test:
    def tested(self,fileName):
        with io.open(fileName, "r",encoding='utf8') as myfile:
            tr_str = myfile.read()
            tr_li = tr_str.split()
            print(tr_str)
        return 0
